var mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017/test-files', {useNewUrlParser: true}, function(err) {
    if(err) {
        console.error('error connecting to db')
    } else {
        console.log('connected to db')
    }
});