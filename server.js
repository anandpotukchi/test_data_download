

const express = require('express')
var bodyParser = require('body-parser')

const app = express()

const port = 3000

const index = require('./src/app/index')
const  data = require('./src/app/data/index')

var connection = require('./config/db')

app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())

app.use('/', index)
app.use('/data', data)

app.listen(port, () => {
    console.log('App running on port', port)
})