var express = require('express')

var router = express.Router()

var Data = require('./data_controller')

router.post('/new', Data.create_data)

router.get('/all', Data.list_data)

router.get('/download', Data.create_document)


module.exports = router