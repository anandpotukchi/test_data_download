var Data = require('./Data.model')
const fs  =require('fs')

var create_data = (req, res) => {

    var body = req.body;

 
    Data.create(body)
    .then( data => {
        res.status(200).json({ message: 'Data inserted', data, status: true })

    }).catch( err=> {
        res.status(400).json({ message: err,  status: true })
    })

},

list_data = (req,res) => {

    new Promise((resolve, reject) => {
       
       resolve(null)
    }).then(() => {

       Data.find({}).then( data => {

           res.status(200).json({ message: 'Data updated', data, status: true })

       }).catch( err => {
            res.status(400).json({ message: err, status: false })
       })
    }).catch( err => {
        res.status(400).json({ message: err, status: false })
    })

},

create_document = ((req,res) => {

    new Promise((resolve, reject) => {
        resolve(null)
    }).then(() => {

        Data.find({}).then( data => {
           
            console.log(data)

            data.map((record) => {

                    const line = record.customerID + '   ' + record.bankAcc + '  ' + record.channel_name + '  ' + record.active +  '\n'
               
               
                    fs.appendFile("file.txt", line, function(err) {
                    if(err) {
                        return console.log(err);
                    }
                    
                    res.download('file.txt')
                });
            })

        }).catch( err => {
            res.status(400).json({ err: err, status: false })
        })

    }).catch( err => {
        res.status(400).json({ message: err, status: false })
    })

})

module.exports = {
    create_data,
    list_data,
    create_document

}